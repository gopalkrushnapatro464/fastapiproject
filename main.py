from typing import Optional
from fastapi import FastAPI

app = FastAPI()

@app.get("/")
def read_root():
    return {"Hello" : "world"}

@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    print("Item is ",item_id)
    return "done"
